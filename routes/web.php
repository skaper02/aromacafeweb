<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Vista home
Route::get('/home', 'Administracion\HomeController@index');
// listar productos
Route::get('/productos', 'Administracion\HomeController@productos');
//Agregar un producto
Route::get('/productos/nuevo', 'Administracion\HomeController@nuevoProducto');
//Guardar el nuevo producto
Route::post('/productos/nuevo/guardar', 'Administracion\HomeController@guardarProducto');
//Editar un producto
Route::get('/productos/editar/{id}', 'Administracion\HomeController@editarProducto');
//Guardar productos
Route::post('/productos/guardar', 'Administracion\HomeController@guardarProductoEdit');

Route::get('/productos/detalles/{id}', 'Administracion\HomeController@visualizarProducto');

Route::get('/productos/eliminar/{id}', 'Administracion\HomeController@eliminarProducto');


Route:: get('/pedidos/ordenes/{id_pedido}','Administracion\HomeController@ordenes');
Route:: get('/pedidos/ordenes/eliminar/{id_orden}','Administracion\HomeController@eliminarOrden');

Route:: get('/ordenes','Administracion\HomeController@listaordenes');

//frontend


Route:: get('/pedidos','Administracion\HomeController@pedidos');

//Route:: get('/pedidos/ordenes/{id_pedido}','Administracion\HomeController@ordenes');

/// FRONTEND
Route:: get('/','Clientes\ClientesController@index');
Route:: get('/menu','Clientes\ClientesController@index');
Route:: get('/bebidas','Clientes\ClientesController@bebidas');
Route:: get('/cervezas','Clientes\ClientesController@cervezas');
Route:: get('/agregarorden','Clientes\ClientesController@insertarordenes');


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
