@extends('administracion.home')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
          <div class="card-title">
            <h2>Informacion del producto a editar</h2>
            <br>
            <div class="form-validation">
                <form class="form-valide" action="{{url('/productos/nuevo/guardar')}}" method="post">
                  {{ csrf_field() }}
 <input type="hidden" name="idproducto" id="idproducto" value="{{$producto->id}}">
                  <div class="row">

                      <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-lg-4 " for="val-username">Nombre<span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="val-username" name="nombre_producto" placeholder="Nombre del producto" value="{{($producto->nombre)}}">
                        </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-lg-4 " for="val-currency">Precio <span class="text-danger">*</span>
                          </label>
                          <div class="col-lg-8">
                            <div class="input-group mb-3">
                                              <div class="input-group-prepend"><span class="input-group-text">$</span>
                                              </div>
                                              <input type="text" class="form-control" name="precio_producto" value="{{($producto->precio)}}.00">
                                              <div class="input-group-append"><span class="input-group-text">.00</span>
                                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-4" for="val-skill">Tipo de Producto<span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-8">
                          <select class="form-control" id="val-skill" name="tipo_producto">
                            <option value="">Seleccione</option>
                            <option value="1">Comida</option>
                            <option value="2">Bebida</option>
                            <option value="3">Postre</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-lg-4" for="val-skill">Clasificacion<span class="text-danger">*</span>
                          </label>
                          <div class="col-lg-8">
                              <select class="form-control" id="val-skill" name="Clasificacion_producto">
                                  <option value="">Seleccione</option>
                                  <option value="1">caliente</option>
                                  <option value="2">frio</option>
                                  <option value="3">desayuno</option>
                                  <option value="4">otro</option>
                              </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-lg-4" for="val-skill">Tamaño<span class="text-danger">*</span>
                          </label>
                          <div class="col-lg-8">
                              <select class="form-control" id="val-skill" name="tamaño_producto">
                                  <option value="">Seleccione</option>
                                  <option value="1">no aplica</option>
                                  <option value="2">Grande</option>
                                  <option value="3">Mediano</option>
                              </select>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-lg-4" for="val-skill">descripcion del produto<span class="text-danger">*</span>
                          </label>
                          <div class="col-lg-8">
                            <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">descripcion</span></div>
                                            <textarea class="form-control" name="descripcion_producto" style="margin-top: 0px; margin-bottom: 0px; height: 40px;"> {{$producto->descripcion}}</textarea>
                              </div>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label for=""class="col-lg-4">Cantidad Disponible</label>

                            <div class="col-lg-8">
                                <input type="number" class="form-control" value="{{$producto->cantidad_disponible}}" id="val-username" name="cantidad_producto" placeholder="Cantidad">
                            </div>

                      </div>
                      <div class="form-group row">
                        <label for="" class="col-lg-4">Otras Opciones</label>
                                          <div class="form-group">
                                              <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                        <?php if($producto->estado =="1"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly  checked>Disponible</label>
                                                        <?php endif; ?>

                                                        <?php if($producto->estado =="0"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly  >Disponible</label>
                                                        <?php endif; ?>


                                              </div>
                                              <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                     <?php if($producto->contable =="1"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly checked>Contable</label>
                                                        <?php endif; ?>

                                                        <?php if($producto->contable =="0"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly  >Contable</label>
                                                        <?php endif; ?>
                                              </div>
                                              <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                      <?php if($producto->rembolsable =="1"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly  checked>Rembolsable</label>
                                                        <?php endif; ?>

                                                        <?php if($producto->rembolsable=="0"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly  >Rembolsable</label>
                                                        <?php endif; ?>
                                              </div>
                                          </div>
                      </div>
                      </div>
                      <div class="col-md-6">
                         <h5 class="text-center">Imagen del producto</h5>
                         <div class="card">
                           <div class="contenido">
                             <img id="img-preview" class="img-product" src="{{($producto->foto_url)}}">
                             <input type="hidden" name="id_imagen" id="id_imagen" value="{{$producto->foto_url}}">
                           </div>

                             <div class="card-footer">

                               <div class="progress mb-3" style="height: 18px">
                                       <div  class="progress-bar  bg-info active progress-bar-striped" style="width: 0%;" role="progressbar" id="img-upload-bar"><span class="sr-only">60% Completado</span>
                                       </div>
                               </div>
                               <div class="input-group mb-3">
                                        <div class="input-group-prepend"><span class="input-group-text"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Subir</font></font></span>
                                        </div>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="img-uploader">
                                                <label class="custom-file-label"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Elija el archivo</font></font></label>
                                            </div>
                              </div>

                           </div>
                         </div>
                         <br>
                         <div class="form-group row">
                                       <div class="col-lg-8 ml-auto">
                                   <button type="submit" class="btn  mb-1 btn-primary btn-lg">Registrar</button>
                               </div>
                           </div>
                         </div>
                       </div>


                </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
  <script src="{{asset('js/uploadimages.js')}}" ></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
@endsection
