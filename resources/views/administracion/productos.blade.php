@extends('administracion.home')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
          <h2 class="card-title">Productos</h2>
          <div class="table-responsive">
            <div class="col offset-10">
                <a href="{{asset('productos/nuevo')}}">
                  <button type="button" class="btn mb-1 btn-outline-primary" >
                    <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                      Nuevo</font></font></button>
                </a>
            </div>
              <table class="table table-striped table-bordered zero-configuration">
                  <thead>
                      <tr>
                          <th>Nombre</th>
                          <th>Precio</th>
                          <th>Tipo</th>
                          <th>Clasificacion</th>
                          <th>Tamaño</th>
                          <th>Disponibles</th>
                          <th>Acciones</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($ls_productos as $pro)
                    <tr>
                      <td>{{$pro->nombre}}</td>
                      <td>{{$pro->precio}}</td>
                      <td>{{$pro->tipo}}</td>
                      <td>{{$pro->clasificacion}}</td>
                      <td>{{$pro->tamanio}}</td>
                      <td>{{$pro->cantidad_disponible}}</td>
                      <td>
                            <div class="dropdown custom-dropdown">
                                <div data-toggle="dropdown"><i class="ti-more-alt"></i>
                                </div>
                                <div class="dropdown-menu dropdown-menu-right">
                                  <a class="dropdown-item text-success" href="productos/detalles/{{$pro->id}}"><span class="ti-eye"> </span>Visualizar</a>
                                  <a class="dropdown-item text-warning" href="productos/editar/{{$pro->id}}"><span class="ti-pencil"> </span>Editar</a>
                                  <a class="dropdown-item  text-danger" href="productos/eliminar/{{$pro->id}}"> <span class="ti-trash"> </span>Eliminar</a
                                </div>
                            </div>
                    </td>
                    </tr>
                    @endforeach

                  </tbody>
                  <tfoot>
                      <tr>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Tipo</th>
                        <th>Clasificacion</th>
                        <th>Tamaño</th>
                        <th>Disponibles</th>
                        <th>Acciones</th>
                      </tr>
                  </tfoot>
              </table>
          </div>
      </div>
    </div>

  </div>

</div>
<script src="{{asset('/plugins/tables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/plugins/tables/js/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('/plugins/tables/js/datatable-init/datatable-basic.min.js')}}"></script>




@endsection
