@extends('administracion.home')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
          <h2 class="card-title">Ordenes</h2>
          <div class="table-responsive">
            <div class="col offset-10">
                <a href="{{asset('productos/nuevo')}}">
                  <button type="button" class="btn mb-1 btn-outline-primary" >
                    <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                      Nuevo</font></font></button>
                </a>
            </div>
              <table class="table table-striped table-bordered zero-configuration">
                  <thead>
                      <tr>
                          <th>id Orden</th>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>Subtotal</th>
                          <th>Estado</th>
                          <th>comentario</th>
                          <th>id_complemento</th>
                          <th>Acciones</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($ls_ordenes as $ord)
                    <tr>
                      <td>{{$ord->id}}</td>
                      <td>{{$ord->Producto->nombre}}</td>
                      <td>{{$ord->cantidad}}</td>
                      <td>{{$ord->subtotal}}</td>
                      <td>{{$ord->estado}}</td>
                      <td>{{$ord->comentario}}</td>
                      <td>{{$ord->id_complemento}}</td>

                      <td>
                            <div class="dropdown custom-dropdown">
                                <div data-toggle="dropdown"><i class="ti-more-alt"></i>
                                </div>
                                <div class="dropdown-menu dropdown-menu-right">
                                  <a class="dropdown-item text-success" href="cambiarestado/{{$ord->id}}"><span class="ti-eye"> </span>Cambiar Estado</a>
                                  <a class="dropdown-item text-warning" href="editar/{{$ord->id}}"><span class="ti-pencil"> </span>Editar</a>
                                  <a class="dropdown-item  text-danger" href="eliminar/{{$ord->id}}"> <span class="ti-trash"> </span>Eliminar</a>
                                </div>
                            </div>
                    </td>
                    </tr>
                    @endforeach

                  </tbody>
                  <tfoot>
                      <tr>
                        <th>id</th>
                        <th>id Mesa</th>
                        <th>Estado</th>
                        <th>Tipo de servicio</th>
                        <th>Total de Productos</th>
                        <th>id empleado</th>
                        <th>Fecha y hora</th>
                        <th>Acciones</th>
                      </tr>
                  </tfoot>
              </table>
          </div>
      </div>
    </div>

  </div>

</div>
<script src="{{asset('/plugins/tables/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('/plugins/tables/js/datatable/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('/plugins/tables/js/datatable-init/datatable-basic.min.js')}}"></script>




@endsection
