@extends('administracion.home')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
          <div class="card-title">
            <h2>Informacion general del producto</h2>
            <br>
            <div class="form-validation">
                <form class="form-valide" action="{{url('/productos/nuevo/guardar')}}" method="post">
                  {{ csrf_field() }}
 <input type="hidden" name="idproducto" id="idproducto" value="{{$producto->id}}">
                  <div class="row">

                      <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-lg-4 " for="val-username">Nombre<span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="val-username" name="nombre_producto" placeholder="Nombre del producto" value="{{($producto->nombre)}}" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-lg-4 " for="val-currency">Precio <span class="text-danger">*</span>
                          </label>
                          <div class="col-lg-8">
                            <div class="input-group mb-3">
                                              <div class="input-group-prepend"><span class="input-group-text">$</span>
                                              </div>
                                              <input type="text" class="form-control" name="precio_producto" value="{{($producto->precio)}}.00" readonly>
                                              <div class="input-group-append"><span class="input-group-text">.00</span>
                                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-4" for="val-skill">Tipo de Producto<span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-8">
                            <input type="text" class="form-control" id="val-username" name="nombre_producto" placeholder="Nombre del producto" value="{{($producto->tipo)}}" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-lg-4" for="val-skill">Clasificacion<span class="text-danger">*</span>
                          </label>
                          <div class="col-lg-8">
                            <input type="text" class="form-control" id="val-username" name="nombre_producto" placeholder="Nombre del producto" value="{{($producto->clasificacion)}}" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-lg-4" for="val-skill">Tamaño<span class="text-danger">*</span>
                          </label>
                          <div class="col-lg-8">
                            <input type="text" class="form-control" id="val-username" name="nombre_producto" placeholder="Nombre del producto" value="{{($producto->tamanio)}}" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                          <label class="col-lg-4" for="val-skill">descripcion del produto<span class="text-danger">*</span>
                          </label>
                          <div class="col-lg-8">
                            <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">descripcion</span></div>
                                            <textarea class="form-control" name="descripcion_producto" style="margin-top: 0px; margin-bottom: 0px; height: 40px;" readonly> {{$producto->descripcion}}</textarea>
                              </div>
                          </div>
                      </div>
                      <div class="form-group row">
                        <label for=""class="col-lg-4">Cantidad Disponible</label>

                            <div class="col-lg-8">
                                <input type="number" class="form-control" value="{{$producto->cantidad_disponible}}" id="val-username" name="cantidad_producto" placeholder="Cantidad" readonly>
                            </div>

                      </div>
                      <div class="form-group row">
                        <label for="" class="col-lg-4">Otras Opciones</label>
                                          <div class="form-group">
                                              <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                        <?php if($producto->estado =="1"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly disabled checked>Disponible</label>
                                                        <?php endif; ?>

                                                        <?php if($producto->estado =="0"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly disabled >Disponible</label>
                                                        <?php endif; ?>


                                              </div>
                                              <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                     <?php if($producto->contable =="1"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly disabled checked>Contable</label>
                                                        <?php endif; ?>

                                                        <?php if($producto->contable =="0"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly disabled >Contable</label>
                                                        <?php endif; ?>
                                              </div>
                                              <div class="form-check form-check-inline">
                                                  <label class="form-check-label">
                                                      <?php if($producto->rembolsable =="1"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly disabled checked>Rembolsable</label>
                                                        <?php endif; ?>

                                                        <?php if($producto->rembolsable=="0"): ?>
                                                        <input type="checkbox" name="disponible" class="form-check-input" value="true" readonly disabled >Rembolsable</label>
                                                        <?php endif; ?>
                                              </div>
                                          </div>
                      </div>
                      </div>
                      <div class="col-md-6">
                         <h5 class="text-center">Imagen del producto</h5>
                         <div class="card">
                           <div class="contenido">
                             <img id="img-preview" class="img-product" src="{{($producto->foto_url)}}">
                             <input type="hidden" name="id_imagen" id="id_imagen" value="{{$producto->foto_url}}">
                           </div>

                             <div class="card-footer">

                               <div class="progress mb-3" style="height: 18px">
                                       <div  class="progress-bar  bg-info active progress-bar-striped" style="width: 0%;" role="progressbar" id="img-upload-bar"><span class="sr-only">60% Completado</span>
                                       </div>
                               </div>
                               <div class="input-group mb-3">

                              </div>

                           </div>
                         </div>
                         <br>
                         <div class="form-group row">
                                       <div class="col-lg-8 ml-auto">
                                         <a href="{{url('/productos/')}}">
                                            <button type="button" class="btn  mb-1 btn-primary btn-lg">Regresar</button>
                                         </a>

                               </div>
                           </div>
                         </div>
                       </div>


                </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
  <script src="{{asset('js/uploadimages.js')}}" ></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
@endsection
