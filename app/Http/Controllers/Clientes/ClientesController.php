<?php
namespace App\http\Controllers\Clientes;
use App\Http\Controllers\Controller;
use App\Models\Productos;
use App\Models\Ordenes;
use App\Models\Pedidos;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;



class ClientesController extends Controller
{
  public function index()
  {
    $bebidas= Productos::select('id','nombre','precio','foto_url')->where('tipo','bebida')->get();
    $pizzas =Productos::select('id','nombre','precio','foto_url')->where('tipo','comida')->get();
    $comidas=Productos::select('id','nombre','precio','foto_url')
                ->where('nombre','LIKE','%pizza%')
                ->get();
    return view('cliente/Home',compact('pizzas',$pizzas,'comidas',$comidas,'bebidas',$bebidas));
  }
  public function insertarordenes(Request $request)
  {
    $idpedido=\Session::get('idCarrito');
    $idproducto=$request->input('idproducto');
    $producto=Productos::find($idproducto);
    $precioproducto= $producto->precio;
    $orden= Ordenes::create(['id_pedido'=>$idpedido,'id_producto'=>$idproducto,'cantidad'=>1,'subtotal'=>$precioproducto,'estado'=>'en espera','comentario'=>'ninguno']);
    return back();
  }
  public function todosLosProductos(){
    $productos= Productos::all();
    return view('cliente/home')->with('ls_productos',$productos);
  }


  public function bebidas()
  {
    $productos= Productos::select('id','nombre','precio','foto_url')->where('tipo','bebida')->get();
    return $productos;
  }
  public function comidas()
  {
    $productos= Productos::select('id','nombre','precio','foto_url')->where('tipo','comida')->get();
    return $productos;
  }
  public function cervezas(){
    $productos= Productos::select('id','nombre','precio','foto_url')
                ->where('nombre','LIKE','%cerveza%')
                ->get();
    return $productos;

  }
  public function pizzas(){
    $productos= Productos::select('id','nombre','precio','foto_url')
                ->where('nombre','LIKE','%pizza%')
                ->get();
    return $productos;

  }





}

 ?>
