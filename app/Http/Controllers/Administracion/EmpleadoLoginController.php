<?php

namespace App\Http\Controllers\Administracion;

use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\AuthenticatesUsers as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdministratorController extends Authenticatable
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admins/area';

    protected function guard()
    {
        return Auth::guard('admins');
    }

    function showLoginForm()
    {
        return view('auth.ad'); //Lugar donde esta el Login de administradores
    }

}
