<?php
namespace App\http\Controllers\Administracion;
use App\Http\Controllers\Controller;
use App\Models\Productos;
use App\Models\Pedidos;
use App\Models\Ordenes;






use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


/**
 *
 */
class HomeController extends Controller
{
  public function index(){
    return view('administracion/home');
  }
  public function productos()
  {

    $listado_productos= Productos::all()->where('estado','1');
    return view('administracion/productos')->with('ls_productos',$listado_productos);


  }
  public function nuevoProducto(){
    return view('administracion/nuevoProducto');
  }
  public function guardarProducto(Request $request){
    $producto = new Productos();

    $token = "946045712:AAHDd9S3thjm9u90eZRjiJWNGuJSCCZ0UAo";
        $id = "787925041";
        $urlMsg = "https://api.telegram.org/bot{$token}/sendMessage";

        $pro = $request->input('nombre_producto');
        $precio =$request->input('precio_producto');
        $tipo=$request->input('tipo_producto');
        $clasificacion=$request->input('Clasificacion_producto');
        $tamano=$request->input('tamaño_producto');
        $descripcion=$request->input('descripcion_producto');
        $cantidad=$request->input('cantidad_producto');

        $msg = "Se agrego un nuevo producto \n
        <b>Nombre del producto:</b> $pro \n
        <b>Precio$:</b> $precio \n
        <b>Tipo:</b> $tipo \n
        <b>Clasificacion:</b> $clasificacion \n
        <b>Tamaño:</b> $tamano \n
        <b>Descripcion:</b> $descripcion \n
        <b>Cantidad:</b> $cantidad";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $urlMsg);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "chat_id={$id}&parse_mode=HTML&text=$msg");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        curl_close($ch);



    $producto->nombre= $request->input('nombre_producto');
    $producto->precio= $request->input('precio_producto');
    $producto->tipo=$request->input('tipo_producto');
    $producto->clasificacion=$request->input('Clasificacion_producto');
    $producto->tamanio=$request->input('tamaño_producto');
    $producto->descripcion=$request->input('descripcion_producto');
    $producto->cantidad_disponible=$request->input('cantidad_producto');
    $producto->foto_url = $request->input('id_imagen');
    $producto->descripcion= $request->input('descripcion_producto');
    if($request->input('disponible')){

      $producto->estado=true;
    }else{
      $producto->estado=false;
    }
    if($request->input('contable')){

      $producto->contable=true;
    }else{
      $producto->contable=false;
    }
    if($request->input('rembolsable')){

      $producto->rembolsable=true;
    }else{
      $producto->rembolsable=false;
    };

    $producto->save();
    return redirect()->to('/productos');



   }

  public function editarProducto($id){
    $producto= Productos::find($id);
    return view('administracion/editarProducto')->with('producto', $producto);
  }
  public function guardarProductoEdit(Request $request)
  {
        $producto= Productos::find($request->id);
        $producto->nombre= $request->input('nombre_producto');
        $producto->precio= $request->input('precio_producto');
        $producto->tipo=$request->input('tipo_producto');
        $producto->clasificacion=$request->input('Clasificacion_producto');
        $producto->tamanio=$request->input('tamaño_producto');
        $producto->descripcion=$request->input('descripcion_producto');
        $producto->cantidad_disponible=$request->input('cantidad_producto');
        $producto->foto_url = $request->input('id_imagen');
        $producto->descripcion= $request->input('descripcion_producto');
        if($request->input('disponible')){

          $producto->estado=true;
        }else{
          $producto->estado=false;
        }
        if($request->input('contable')){

          $producto->contable=true;
        }else{
          $producto->contable=false;
        }
        if($request->input('rembolsable')){

          $producto->rembolsable=true;
        }else{
          $producto->rembolsable=false;
        };

        $producto->save();
        return productos();
  }
  public function visualizarProducto($id)
  {
    $producto= Productos::find($id);
    return view('administracion/visualizarProducto')->with('producto', $producto);
  }
 /////// Pedidos logica
  public function pedidos()
  {
    $pedidos=Pedidos::all();
    return view('administracion/pedidos')->with('ls_pedidos',$pedidos);
  }

  ////// Ordenes logica
  public function listaordenes()
  {
    $ordenes= Ordenes::all();
    return view('administracion/ordenes')->with('ls_ordenes',$ordenes);

  }

  public function ordenes($idpedido)
  {
    $ordenes= Pedidos::find($idpedido)->Ordenes;

   return view('administracion/ordenesdelpedido')->with('ls_ordenes',$ordenes);
  }
  public function eliminarOrden($idorden){

    $orden= Ordenes::find($idorden);
    $orden->delete();
    return back();

  }



  public function eliminarProducto($id)
  {
     $actualizar = Productos::find($id);
              //          nombr en BD         nombre de identificador en html
              $actualizar->estado = '0';
              $actualizar->save();

              return redirect()->to('/productos');
  }


}
 ?>
