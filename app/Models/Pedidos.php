<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ordenes;

class Pedidos extends Model
{

  public function Ordenes()
  {
    return $this->hasMany(Ordenes::class,'id_pedido');
  }
  public function conteoProductos()
  {
       return $this->Ordenes()->count();
  }


  protected $table= 'pedidos';
  protected $primarykey ="id";
  public $timestamps= true;
  protected $fillable=['id_mesa','estado',
  'tipo_servicio','total_productos','id_empleado','created_at'];

  public static function crearEncontrarporSession($idpedido){
    	if ($idpedido){
    		return Pedidos::buscarPorSession($idpedido);
    	}else{
    		return Pedidos::crearCarritoConSession();
    	}
	}

	public static  function buscarPorSession($idpedido){
	return Pedidos::find($idpedido);
	}
	public static function crearCarritoConSession(){
	return Pedidos::create(['estado'=>'incompleto']);
	}

}
