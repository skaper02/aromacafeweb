<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Productos;
use App\Models\Pedidos;


class Ordenes extends Model
{

public function Producto()
{
  return $this->belongsTo(Productos::class,'id_producto');

}

  protected $table= 'ordenes';

protected $primarykey ="id";
public $timestamps= true;
protected $fillable=['id_pedido','id_producto',
'cantidad','subtotal','estado','comentario',
'id_complemento'];
}
