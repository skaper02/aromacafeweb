<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
  protected $table= 'empleados';

protected $primarykey ="id";
public $timestamps= false;
protected $fillable=['id','name','nombre','apellidos','email','telefono','domicilio','turno','password'];

}
