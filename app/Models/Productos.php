<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ordenes;

class Productos extends Model
{


  protected $table= 'productos';

protected $primarykey ="id";
public $timestamps= false;
protected $fillable=['nombre','precio',
'tipo','clasificacion','tamanio','descripcion',
'cantidad_disponible','estado','contable',
'rembolsable','foto_url'];
}
