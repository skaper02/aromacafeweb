<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Pedidos;

class PedidosProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      view()->composer("*",function($view){
          $idCarrito = \Session::get('idCarrito');
          $carrito= Pedidos::crearEncontrarporSession($idCarrito);
           \Session::put('idCarrito',$carrito->id);
           $view->with('carrito',$carrito);
      });
    }
}
