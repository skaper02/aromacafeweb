const cloudName = '<aroma></aroma>cafe';
const unsignedUploadPreset = 'fyimpthp';

$.cloudinary.config({ cloud_name: "demo"});

$('.cloudinary_fileupload').unsigned_cloudinary_upload(unsignedUploadPreset, {
    cloud_name: cloudName,
    tags: 'browser_uploads'
  }, {
    multiple: true
  }
  )
  .bind('cloudinarydone', function(e, data) {
    console.log(`data.loaded: ${data.loaded},
  data.total: ${data.total}`)
  })
  .bind('fileuploadprogress', function(e, data) {
    console.log(`fileuploadprogress data.loaded: ${data.loaded},
  data.total: ${data.total}`);
  })
  .bind('cloudinaryprogress', function(e, data) {
    console.log(`cloudinaryprogress data.loaded: ${data.loaded},
  data.total: ${data.total}`);
  })
  .bind('cloudinarydone', function(e, data) {
  // inspect data.result for return value with link to the uploaded image and more
  console.log('Upload result', data.result);
  // Create a thumbnail of the uploaded image, with 150px width
    var image = $.cloudinary.image(
        data.result.public_id, {
          secure: true,
          width: 150,
          crop: 'scale'
        });
    $('.gallery').prepend(image);
});

/*
Sample output in browser console:

cloudinaryprogress data.loaded: 1228664,
  data.total: 4620250
VM367:86 fileuploadprogress data.loaded: 1228664,
  data.total: 4620250
VM426:90 cloudinaryprogress data.loaded: 3751521,
  data.total: 4620250
VM367:86 fileuploadprogress data.loaded: 3751521,
  data.total: 4620250
VM426:90 cloudinaryprogress data.loaded: 4341280,
  data.total: 4620250
VM367:86 fileuploadprogress data.loaded: 4341280,
  data.total: 4620250
VM426:90 cloudinaryprogress data.loaded: 4620250,
  data.total: 4620250
VM367:86 fileuploadprogress data.loaded: 4620250,
  data.total: 4620250
VM339:82 data.loaded: 4620250,
  data.total: 4620250

*/
