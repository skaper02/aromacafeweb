<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_mesa')->nullable();
            $table->enum('estado',['incompleto','aprobado','abierto','cerrado','pagado']);
            $table->enum('tipo_servicio',['aqui','domicilio','para llevar'])->nullable();
            $table->integer('total_productos')->default('0');
            $table->integer('id_empleado')->default('1');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
