<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',25);
            $table->double('precio', 8, 2);
            $table->enum('tipo',['comida','bebida','postre']);
            $table->enum('clasificacion',['caliente','fria','desayuno','otro']);
            $table->enum('tamanio',['no aplica','Grande','Mediano']);
            $table->string('descripcion');
            $table->integer('cantidad_disponible');
            $table->boolean('estado');
            $table->boolean('contable');
            $table->boolean('rembolsable');
            $table->string('foto_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
